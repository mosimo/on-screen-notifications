﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace OnScreenNotifications
{
    public partial class Form1 : Form
    {

        protected override bool ShowWithoutActivation
        {
            get { return true; }
        }

            [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect, // x-coordinate of upper-left corner
            int nTopRect, // y-coordinate of upper-left corner
            int nRightRect, // x-coordinate of lower-right corner
            int nBottomRect, // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
         );

        public Form1(string s)
        {
            InitializeComponent();
            label1.Text = s;
            this.Width = label1.Width + 38;

            this.StartPosition = FormStartPosition.Manual;
            int x = (Screen.PrimaryScreen.Bounds.Width / 2) - (this.Width /2);
            int y = (Screen.PrimaryScreen.Bounds.Height - 150);

            

            
            this.Location = new Point(x, y);
            
            this.FormBorderStyle = FormBorderStyle.None;
            this.Opacity = 0.85;
            

            

            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Close();
        }
        
    }


}
