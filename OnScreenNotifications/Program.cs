﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace OnScreenNotifications
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                if (args.Length == 2)
                {
                    runProg(args[1]);
                }
                else
                {
                    if (args.Length == 3)
                    {
                        runProg(args[1], args[2]);
                    }
                }

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1(args[0]));
                
            }
                
        }

        public static void runProg(string program)
        {
            Process RunProcess = new Process();

            try
            {
                RunProcess.StartInfo.UseShellExecute = false;
                RunProcess.StartInfo.WorkingDirectory = Path.GetDirectoryName(program);
                RunProcess.StartInfo.FileName = program;
                RunProcess.Start();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void runProg(string program, string args)
        {
            Process RunProcess = new Process();

            try
            {
                RunProcess.StartInfo.UseShellExecute = false;
                RunProcess.StartInfo.WorkingDirectory = Path.GetDirectoryName(program);
                RunProcess.StartInfo.FileName = program;
                RunProcess.StartInfo.Arguments = args;
                RunProcess.Start();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
