# README #

Display on screen notifications and run a program.

### How do I use it ###

* Extract OnScreenNotifications.exe somewhere
* Run using: OnScreenNotifications.exe <text> <program> <args [optional]>
* * Text: The text to display in the notification
* * Program: The program to run
* * Args: Any arguments to send to the program you're running.

* If your text, program or args contain spaces enclose each in quotes "